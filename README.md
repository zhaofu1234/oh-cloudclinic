# oh-cloudclinic

#### 介绍
开源医疗-云诊所

[功能清单](https://doc.openhis.org.cn/docs/%E4%BA%A7%E5%93%81%E8%B5%84%E6%96%99/%E4%BA%A7%E5%93%81%E5%8A%9F%E8%83%BD%E4%BB%8B%E7%BB%8D%EF%BC%88%E5%8C%BB%E7%96%97%EF%BC%89/OpenHIS%E8%AF%8A%E6%89%80%E7%B3%BB%E7%BB%9F) https://doc.openhis.org.cn/docs/%E4%BA%A7%E5%93%81%E8%B5%84%E6%96%99/%E4%BA%A7%E5%93%81%E5%8A%9F%E8%83%BD%E4%BB%8B%E7%BB%8D%EF%BC%88%E5%8C%BB%E7%96%97%EF%BC%89/OpenHIS%E8%AF%8A%E6%89%80%E7%B3%BB%E7%BB%9F

[开发手册](https://doc.openhis.org.cn/docs/%E5%BC%80%E5%8F%91%E6%89%8B%E5%86%8C/%E5%BC%80%E6%BA%90%E5%8C%BB%E7%96%97/%E8%AF%8A%E6%89%80%E7%89%88HIS%E5%BC%80%E5%8F%91%E6%89%8B%E5%86%8C) https://doc.openhis.org.cn/docs/%E5%BC%80%E5%8F%91%E6%89%8B%E5%86%8C/%E5%BC%80%E6%BA%90%E5%8C%BB%E7%96%97/%E8%AF%8A%E6%89%80%E7%89%88HIS%E5%BC%80%E5%8F%91%E6%89%8B%E5%86%8C

[部署手册](https://doc.openhis.org.cn/docs/%E9%83%A8%E7%BD%B2%E6%89%8B%E5%86%8C/%E5%BC%80%E6%BA%90%E5%8C%BB%E7%96%97/%E8%AF%8A%E6%89%80%E7%89%88HIS/%E8%AF%8A%E6%89%80%E7%89%88HIS%E9%83%A8%E7%BD%B2%E6%89%8B%E5%86%8C) https://doc.openhis.org.cn/docs/%E9%83%A8%E7%BD%B2%E6%89%8B%E5%86%8C/%E5%BC%80%E6%BA%90%E5%8C%BB%E7%96%97/%E8%AF%8A%E6%89%80%E7%89%88HIS/%E8%AF%8A%E6%89%80%E7%89%88HIS%E9%83%A8%E7%BD%B2%E6%89%8B%E5%86%8C

[操作手册](https://doc.openhis.org.cn/docs/%E6%93%8D%E4%BD%9C%E6%89%8B%E5%86%8C/%E5%BC%80%E6%BA%90%E5%8C%BB%E7%96%97/%E8%AF%8A%E6%89%80%E7%89%88HIS%E6%93%8D%E4%BD%9C%E6%89%8B%E5%86%8C) https://doc.openhis.org.cn/docs/%E6%93%8D%E4%BD%9C%E6%89%8B%E5%86%8C/%E5%BC%80%E6%BA%90%E5%8C%BB%E7%96%97/%E8%AF%8A%E6%89%80%E7%89%88HIS%E6%93%8D%E4%BD%9C%E6%89%8B%E5%86%8C


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

